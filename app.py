from flask import Flask, jsonify, request
import os

app = Flask(__name__)

PORT = os.getenv("PORT")

# http://localhost:9998/hello
@app.route("/hello/<name>", methods=["GET"])
def get_hello(name):
    return jsonify({"hello":name})

@app.route("/number/<integer>", methods=["POST"])
def create_number(integer):
    body = request.json
    return jsonify({"result":integer, "body":body})

if __name__ == "__main__":
    app.run(debug=True, port=PORT, host="0.0.0.0")